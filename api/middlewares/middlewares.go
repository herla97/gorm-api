package middlewares

import (
	"errors"
	"net/http"

	"gitlab.com/herla97/gorm-api/api/auth"
	"gitlab.com/herla97/gorm-api/api/responses"
)

// SetMiddlewareJSON Agrega formato a todas las respuestas JSON.
func SetMiddlewareJSON(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next(w, r)
	}
}

// SetMiddlewareAuthentication Auteticación del Token.
func SetMiddlewareAuthentication(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := auth.TokenValid(r)
		if err != nil {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
			return
		}
		next(w, r)
	}
}
